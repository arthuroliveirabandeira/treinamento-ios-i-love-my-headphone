

import UIKit

class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var feeds : [entidadeFeed] = []
    var loading : UIAlertView!
    
    @IBAction func logout(sender: AnyObject) {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.removeObjectForKey("token")
        
        self.performSegueWithIdentifier("login", sender: self)
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.feeds.count
    }
    
   
    @IBOutlet var feedTableView: UITableView!
    
    
    
    override func viewDidLoad() {
        
        
       self.title = "Feed de Avaliacoes"
        
        let defaults = NSUserDefaults.standardUserDefaults()
        var token = defaults.objectForKey("token") as! String?
        
          if token == nil{
            self.performSegueWithIdentifier("login", sender: self)
          }else{
         loading = UIAlertView()
        loading.message = "Buscando Avaliacoes..."
        loading.show()
        self.gettingFeed()

        }

    }
    
    func gettingFeed(){
        
        var manager = AFHTTPRequestOperationManager()
        
        manager.GET("HTTP://45.55.74.184:8080/publications/list/", parameters: nil, success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject) -> Void in
            
            self.loading.dismissWithClickedButtonIndex(0, animated: true)
            
            var objects : [NSDictionary] = responseObject.objectForKey("publications") as!
                [NSDictionary]
            for object:NSDictionary in objects {
                
                var review = entidadeFeed()
                review.marca = object["brand"] as? String
                review.modelo = object["model"] as? String
                review.rating  = object["rating"] as? Int
                review.descricao = object ["comment"] as? String
                review.foto = object["photo"] as? String
                self.feeds.append(review)
                
                
            }
            self.feedTableView.reloadData()
            self.loading.dismissWithClickedButtonIndex(0, animated: true)
            
            }) { (operation:AFHTTPRequestOperation!, error:NSError) -> Void in
                
                let alert = UIAlertView()
                alert.title = "Erro!"
                alert.message = "Erro interno do servidor!"
                alert.addButtonWithTitle("Ok")
                alert.show()
                self.loading.dismissWithClickedButtonIndex(0, animated: true)
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.removeObjectForKey("token")
        }
        
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        var cell = tableView.dequeueReusableCellWithIdentifier("FoneDetalhe") as! FoneCelulaTableViewCell
        cell.foneDescricao?.text = self.feeds[indexPath.row].descricao
        cell.foneMarca?.text = self.feeds[indexPath.row].marca
        cell.foneModelo?.text = self.feeds[indexPath.row].modelo
        cell.foneFoto!.setImageWithUrl(NSURL(string: self.feeds[indexPath.row].foto!)! , placeHolderImage : UIImage(named: "btn-photo-upload"))
        cell.foneRating.image = UIImage(named: "rating-\(self.feeds[indexPath.row].rating!)-stars")                                                    //        cell.foneRating? = self.feeds [indexPath]
        cell.foneFoto.layer.cornerRadius = 0.5 * cell.foneFoto.bounds.size.width

        
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "verReview"{
            var index = self.feedTableView.indexPathForSelectedRow()
            var feed : entidadeFeed = self.feeds[index!.row]
            var detalheController = segue.destinationViewController as! Detalhe_PostViewController
            detalheController.review = feed
            detalheController.onlyShow = true
        }
    }
  
}
