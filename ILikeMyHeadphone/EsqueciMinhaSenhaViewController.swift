//
//  EsqueciMinhaSenhaViewController.swift
//  ILikeMyHeadphone
//
//  Created by aluno-r17 on 7/13/15.
//  Copyright (c) 2015 Deway. All rights reserved.
//

import UIKit

class EsqueciMinhaSenhaViewController: UIViewController {

    @IBOutlet var emailTxtField: UITextField!
   
    var recuperando : UIAlertView!
    
    @IBAction func recuperar(sender: AnyObject) {
        if !isValidEmail(self.emailTxtField.text){
            var alert = UIAlertView()
            alert.title = "ALERTA!"
            alert.message = "Email Invalido"
            alert.addButtonWithTitle("Ok")
            alert.show()
            return
            
        }
        recuperando = UIAlertView()
        recuperando.message = "Recuperando senha..."
        recuperando.show()
        self.makingRetrieve()
    }
    
    func makingRetrieve(){
        var manager = AFHTTPRequestOperationManager()
        var param = ["email": self.emailTxtField.text]
        
        manager.POST("HTTP://45.55.74.184:8080/sign/request_reset_password/", parameters: param, success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject) -> Void in
            
            self.recuperando.dismissWithClickedButtonIndex(0, animated: true)
            
            var alert = UIAlertView()
            alert.message = "Senha reenviada para o email"
            alert.title = "Sucesso!"
            alert.addButtonWithTitle("Ok")
            
            //
            }) { (operation:AFHTTPRequestOperation!, error:NSError) -> Void in
                
                let alert = UIAlertView()
                alert.title = "Erro!"
                alert.message = operation.responseString
                alert.addButtonWithTitle("Ok")
                alert.show()
                self.recuperando.dismissWithClickedButtonIndex(0, animated: true)
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.removeObjectForKey("token")
                
                //
        }
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }

        
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
