//
//  CadastroViewController.swift
//  ILikeMyHeadphone
//
//  Created by aluno-r17 on 7/13/15.
//  Copyright (c) 2015 Deway. All rights reserved.
//

import UIKit

class CadastroViewController: UIViewController {

    @IBOutlet var nomeTxtField: UITextField!
    @IBOutlet var emailTxtField: UITextField!
    @IBOutlet var senhaTxtField: UITextField!
    var registrando : UIAlertView!
    @IBAction func cadastrar(sender: AnyObject) {
        if (nomeTxtField == ""){
        let alert = UIAlertView()
            alert.title = "Campo de Nome Vazio"
            alert.message = "Preencha os Campos!"
            alert.addButtonWithTitle("Ok")
        }
        if !isValidEmail(self.emailTxtField.text){
            var alert = UIAlertView()
            alert.title = "ALERTA!"
            alert.message = "Email Invalido"
            alert.addButtonWithTitle("Ok")
            alert.show()
            return
            
        }
        
        if count(senhaTxtField.text) < 4{
            var alert  = UIAlertView()
            alert.message = "Senha Pequena Demais"
            alert.title = "ALERTA"
            alert.addButtonWithTitle("Ok")
            alert.show()
            return
        }

        registrando = UIAlertView()
        registrando.message = "Realizando cadastro..."
        registrando.show()
        self.makingCadastro()
    
    }
    func makingCadastro (){
        var manager = AFHTTPRequestOperationManager()
        var param = ["email": self.emailTxtField.text, "password" : self.senhaTxtField.text , "username" : self.nomeTxtField.text ]
        
        manager.POST("HTTP://45.55.74.184:8080/users/sign_up/", parameters: param, success: { (operation:AFHTTPRequestOperation!, responseObject:AnyObject) -> Void in
            
            self.registrando.dismissWithClickedButtonIndex(0, animated: true)
            var token = responseObject.objectForKey("token") as! String
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(token, forKey: "token")
            self.dismissViewControllerAnimated(true, completion: nil)
            
            
            
            
            //
            }) { (operation:AFHTTPRequestOperation!, error:NSError) -> Void in
                
                let alert = UIAlertView()
                alert.title = "Erro!"
                alert.message = "Login ou Senha invalidos!"
                alert.addButtonWithTitle("Ok")
                alert.show()
                self.registrando.dismissWithClickedButtonIndex(0, animated: true)
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.removeObjectForKey("token")
                
                //
        }

    
    }
    
    
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
