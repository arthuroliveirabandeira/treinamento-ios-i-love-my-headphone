//
//  Detalhe_PostViewController.swift
//  ILikeMyHeadphone
//
//  Created by aluno-r17 on 7/14/15.
//  Copyright (c) 2015 Deway. All rights reserved.
//

import UIKit

class Detalhe_PostViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    
    @IBOutlet weak var imageButton : UIButton!
    @IBOutlet weak var marca       : UITextField!
    @IBOutlet weak var modelo      : UITextField!
   
    @IBOutlet var enviarButton: UIButton!
    @IBOutlet var ratingButton: UIButton!
    @IBOutlet weak var avaliacao   : UITextField!
   
    //subir teclado
    @IBOutlet weak var topButtonConstraint: NSLayoutConstraint!
    // -----
    
    // imagepicker
    
    @IBAction func imagePickerEscolher(sender: AnyObject) {
        
        self.imagepicker = UIImagePickerController()
        self.imagepicker.delegate = self
        self.imagepicker.sourceType = .SavedPhotosAlbum
        self.presentViewController(self.imagepicker, animated: true, completion: nil)
        
        
    
    }
    var imagepicker :UIImagePickerController!
    var imageURL    : String!
    var loading     : UIAlertView!
    var onlyShow    : Bool!
    var review      : entidadeFeed!
    var rating      : Int!
    
    @IBAction func changeRating(sender: AnyObject) {
        if self.rating == 5 {
        self.rating = 1
        }else{
        self.rating + 1
            
        }
        ratingButton.setImage(UIImage(named: "rating-\(self.rating)-stars"), forState: .Normal)
    
    }
    
    
    @IBAction func enviar(sender: AnyObject) {
        downKeyBoard()
        self.loading = UIAlertView()
        self.loading.message = "Enviado Review..."
        self.loading.show()
        
        self.sendReview()
        
        
        
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
          var tap = UITapGestureRecognizer()
          tap.addTarget(self, action: Selector("downKeyBoard"))
            self.view.addGestureRecognizer(tap)
            self.rating = 5
            NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWasShown:"), name:UIKeyboardWillShowNotification, object: nil);
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        
        if onlyShow != nil{
            self.marca.text = self.review.marca!
            self.modelo.text = self.review.modelo!
            self.avaliacao.text = self.review.descricao!
            
            
            let url = NSURL(string : self.review.foto!)
            let data = NSData(contentsOfURL: url!)
            self.imageButton.setImage(UIImage(data: data!), forState : .Normal)
            self.imageButton.imageView?.layer.cornerRadius = 0.5 * imageButton.bounds.size.width
          
            
            var ratingName = "rating-\(review.rating!)-stars"
            self.ratingButton.setImage(UIImage(named: ratingName), forState: .Normal)
            
            self.enviarButton.hidden = true
            self.view.userInteractionEnabled = false
            
            self.title = "\(marca.text) \(modelo.text) "
            
            
           
        } else{
            self.title = "Nova Review"
          }


       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // function for the imagepicker and upload of images
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        self.imagepicker.dismissViewControllerAnimated(true, completion: nil)
        
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        let imageJPEG = UIImageJPEGRepresentation(image, 1.0)
        
        self.imageButton.layer.cornerRadius =   0.5 * imageButton.bounds.size.width
        self.imageButton.clipsToBounds = true;
        self.imageButton.setImage(image, forState: UIControlState.Normal)
        
        
        
        var manager = AFHTTPRequestOperationManager()
        
        manager.POST( "http://deway.com.br/treinamentos/unifor/upload.php", parameters: nil,
            constructingBodyWithBlock: { (data: AFMultipartFormData!) in
                data.appendPartWithFileData(imageJPEG, name: "file", fileName: "foto.jpg", mimeType: "image/jpeg")
                
            },
            success: { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in
                self.imageURL = responseObject.objectForKey("URL") as! String
            },
            failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in
                println("Erro.. \(error.localizedDescription)")
        })
        
    }
    //------------------------------------------------------
    func sendReview() {
        var params = ["photo_url": self.imageURL, "brand": self.marca.text, "model": self.modelo.text, "comment": self.avaliacao.text, "rating": "\(self.rating)"]
        
        var manager = AFHTTPRequestOperationManager()
        
        manager.requestSerializer.setValue("Token 36d2a0f45809a7779f2693fd5f62a5674a01d488", forHTTPHeaderField: "Authorization")
        
        manager.POST("http://45.55.74.184:8080/publications/post/", parameters: params, success: { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) -> Void in
            self.loading.dismissWithClickedButtonIndex(0, animated: true)
            
            let alert = UIAlertView()
            alert.message = "Review enviado com sucesso!"
            alert.addButtonWithTitle("OK")
            alert.show()
            
            self.navigationController?.popToRootViewControllerAnimated(true)
            
            
            }) { (operation:AFHTTPRequestOperation!, error:NSError) -> Void in
                let alert = UIAlertView()
                alert.title = "Erro!"
                alert.message = "Erro interno no servidor."
                alert.addButtonWithTitle("OK")
                alert.show()
                
                self.loading.dismissWithClickedButtonIndex(0, animated: true)
        }
    }

    func downKeyBoard(){
        self.view.endEditing(true)
    }
    
    func keyboardWasShown(notification: NSNotification) {
        var info = notification.userInfo!
        var keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.topButtonConstraint.constant = -115
        })
        
        
    }
    
    
    func keyboardWillHide(notification: NSNotification) {
        var info = notification.userInfo!
        var keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.topButtonConstraint.constant = 14
        })
    }
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
