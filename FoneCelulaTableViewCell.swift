//
//  FoneCelulaTableViewCell.swift
//  ILikeMyHeadphone
//
//  Created by aluno-r17 on 7/10/15.
//  Copyright (c) 2015 Deway. All rights reserved.
//

import UIKit

class FoneCelulaTableViewCell: UITableViewCell {

    @IBOutlet var foneFoto: UIImageView!
    @IBOutlet var foneDescricao: UILabel!
    @IBOutlet var foneRating: UIImageView!
    @IBOutlet var foneMarca: UILabel!
    @IBOutlet var foneModelo: UILabel!
    
        override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
